**README file for GWAS_analyses_Populus repository**

This repository contains example scripts used for the analyses described in the paper "Admixture mapping in interspecific Populus hybrids identifies classes of genomic architectures for phytochemical, morphological, and growth traits" (Bresadola et al., under review). The scripts are divided in three folders.

---

## 1_rawdata_processing

This folder contains scripts for the processing of .fastq files obtained from a RAD-sequencing experiment:

1. 1_demultiplex_and_qualityTrimming.sh: demultiplex and remove bases/reads with bad quality
2. 2_alignment_to_referenceGenome.sh: align the reads to a reference genome
3. 3_add_read_group.sh: add read group information to bam files
4. 4_target_creator.sh: create a file with intervals to be used in GATK Indel Realigner
5. 5_indel_realigner.sh: perform re-alignment around indels
6. 6_bqsr.sh: perform base quality score recalibration
7. 7_UnifiedGenotyper_RASPberry.sh: call genotypes at specific loci, used then in RASPberry
8. 8_UnifiedGenotyper_all.sh: call SNPs and genotypes at all variable sites

---

## 2_ancestry_inference

This folder contains scripts to run analyses related to ancestry inference:

1. 1_ADMIXTURE.sh: run ADMIXTURE
2. 2_input_file_RASPberry.input: input file with parameters to run RASPberry
3. 3_run_RASPberry.sh: run RASPberry
4. Hudson_Fst.r: calculate Hudson Fst from entropy results (code for entropy is in Dryad repository https://doi.org/10.5061/dryad.pq93h - see Gompert et al., 2014 https://doi.org/10.1111/mec.12811 for a description)

---

## 3_GWAS

This folder contains scripts to run analyses related to LD calculation and GWAS:

1. calculate_ancestry_LD_chr09.r: calculate ancestry LD from RASPberry weighted ancestries
2. example_regression_q_cg.r: script to regress out covariates from raw phenotypic measurements
3. quantitative_to_binary_raw_and_resid.r: convert traits with many zeros to binary traits and get residuals from logistic regression
4. gemma_example_bslmm.sh: run bslmm in GEMMA
5. gemma_example_lmm.sh: run lmm in GEMMA
6. gemma_example_notsnp.sh: run bslmm with -notsnp option in GEMMA
7. pheno_variance_explained_by_q.r: calculate proportion of phenotypic variance explained by genome-wide admixture proportion
8. combine_mcmc_chains.r: concatenate results from GEMMA MCMC chains, downsample and calculate PVE*PGE
9. get_prob_mass.r: calculate probability mass and medians from GEMMA results







