# AIM: estimate the proportion of phenotypic variance explained by genome-wide ancestry (q)
# Different covariates have to be taken into account for different sets of traits

setwd("/path/mydirectory/")
library("stringr")

# import file with information about genome-wide ancestry (q), common garden location (cg), planting year (year) and measurements of all phenotypic traits
pheno_all<-read.table("file_with_phenotypic_data.txt", header=TRUE)

# define number of bootstrap repetitions
numBootstraps<-1000;


########## majority of phytochemical traits
########## covariates = q and cg
# function to calculate variance explained by q:
getVarExplained<-function(x,cg,q){
  orig<-var(x)
  res1<-resid(lm(x~cg))
  var1<-var(res1)
  res2<-resid(lm(res1~scale(q)))
  var2<-var(res2)
  ratio2<-var2/var1
  varExpl<-1-ratio2
  return(varExpl); # varExpl is the phenotypic variance explained by q
}

# select columns with cg, q and measurements of phytochemical traits
chem<-pheno_all[,c(2,16,130:165,167,169)]
# remove NAs
chem.na<-na.omit(chem)

chem.results<-matrix(data=NA,ncol=numBootstraps+1,nrow=ncol(chem.na)-2) # -2 because in chem.na there are 2 columns with cg and q
quantiles.chem<-matrix(data=NA,ncol=2,nrow=ncol(chem.na)-2)

for (i in 3:ncol(chem.na)){
  chem.results[i-2,1]<-getVarExplained(chem.na[,i],chem.na$CG,chem.na$q)
  for(r in 1:numBootstraps){
    bs <- sample(1:length(chem.na[,i]),replace=TRUE);
    chem.results[i-2,r+1]<-getVarExplained(chem.na[bs,i],chem.na$CG[bs],chem.na$q[bs])
    }
  quantiles.chem[i-2,]<-quantile(chem.results[i-2,2:ncol(chem.results)],probs=c(0.025, 0.975))
}

quantiles.chem<-as.data.frame(quantiles.chem)
colnames(quantiles.chem)<-c("quantile_2.5","quantile_97.5")
quantiles.chem$orig<-chem.results[,1]
rownames(quantiles.chem)<-colnames(chem.na[3:ncol(chem.na)])
write.table(quantiles.chem,file="my_filename_1.txt",quote=FALSE,col.names=TRUE,row.names=TRUE,sep="\t")


########## C6b and C9ii
########## covariate = q
getVarExplained2<-function(x,q){
  orig<-var(x)
  res1<-resid(lm(x~scale(q)))
  var1<-var(res1)
  ratio1<-var1/orig
  varExpl<-1-ratio1
  return(varExpl);
}

# select columns with cg, q and measurements of phytochemical traits
two<-pheno_all[,c(2,16,166,168)]
# remove NAs
two.na<-na.omit(two)

two.results<-matrix(data=NA,ncol=numBootstraps+1,nrow=ncol(two.na)-2)
quantiles.two<-matrix(data=NA,ncol=2,nrow=ncol(two.na)-2)

for (i in 3:ncol(two.na)){
  two.results[i-2,1]<-getVarExplained2(two.na[,i],two.na$q)
  for(r in 1:numBootstraps){
    bs <- sample(1:length(two.na[,i]),replace=TRUE);
    two.results[i-2,r+1]<-getVarExplained2(two.na[bs,i],two.na$q[bs])
    }
  quantiles.two[i-2,]<-quantile(two.results[i-2,2:ncol(two.results)],probs=c(0.025, 0.975))
}

quantiles.two<-as.data.frame(quantiles.two)
colnames(quantiles.two)<-c("quantile_2.5","quantile_97.5")
quantiles.two$orig<-two.results[,1]
rownames(quantiles.two)<-colnames(two.na[3:ncol(two.na)])
write.table(quantiles.two,file="my_filename_2.txt",quote=FALSE,col.names=TRUE,row.names=TRUE,sep="\t")


########## height and diam first year
########## covariates = q and planting year (year)
getVarExplained3<-function(x,year,q){
  orig<-var(x)
  res1<-resid(lm(x~year))
  var1<-var(res1)
  res2<-resid(lm(res1~scale(q)))
  var2<-var(res2)
  ratio2<-var2/var1
  varExpl<-1-ratio2
  return(varExpl);
}

# select columns with year, q and measurements of growth traits (first year after planting)
first<-pheno_all[,c(3,16,40,56)]
# these columns have different NAs, so they have to be processed separately
first.h<-cbind(na.omit(first[,c(1,2,3)]))
first.d<-cbind(na.omit(first[,c(1,2,4)]))

first.h.results<-matrix(data=NA,ncol=numBootstraps+1,nrow=ncol(first.h)-2)
quantiles.first.h<-matrix(data=NA,ncol=2,nrow=ncol(first.h)-2)

for (i in 3:ncol(first.h)){
  first.h.results[i-2,1]<-getVarExplained3(first.h[,i],first.h$planting_year,first.h$q)
  for(r in 1:numBootstraps){
    bs <- sample(1:length(first.h[,i]),replace=TRUE);
    first.h.results[i-2,r+1]<-getVarExplained3(first.h[bs,i],first.h$planting_year[bs],first.h$q[bs])
    }
  quantiles.first.h[i-2,]<-quantile(first.h.results[i-2,2:ncol(first.h.results)],probs=c(0.025, 0.975))
}

quantiles.first.h<-as.data.frame(quantiles.first.h)
colnames(quantiles.first.h)<-c("quantile_2.5","quantile_97.5")
quantiles.first.h$orig<-first.h.results[,1]
rownames(quantiles.first.h)<-colnames(first.h[3:ncol(first.h)])
write.table(quantiles.first.h,file="my_filename_3.txt",quote=FALSE,col.names=TRUE,row.names=TRUE,sep="\t")


first.d.results<-matrix(data=NA,ncol=numBootstraps+1,nrow=ncol(first.d)-2)
quantiles.first.d<-matrix(data=NA,ncol=2,nrow=ncol(first.d)-2)

for (i in 3:ncol(first.d)){
  first.d.results[i-2,1]<-getVarExplained3(first.d[,i],first.d$planting_year,first.d$q)
  for(r in 1:numBootstraps){
    bs <- sample(1:length(first.d[,i]),replace=TRUE);
    first.d.results[i-2,r+1]<-getVarExplained3(first.d[bs,i],first.d$planting_year[bs],first.d$q[bs])
    }
  quantiles.first.d[i-2,]<-quantile(first.d.results[i-2,2:ncol(first.d.results)],probs=c(0.025, 0.975))
}

quantiles.first.d<-as.data.frame(quantiles.first.d)
colnames(quantiles.first.d)<-c("quantile_2.5","quantile_97.5")
quantiles.first.d$orig<-first.d.results[,1]
rownames(quantiles.first.d)<-colnames(first.d[3:ncol(first.d)])
write.table(quantiles.first.d,file="my_filename_4.txt",quote=FALSE,col.names=TRUE,row.names=TRUE,sep="\t")



########### height and diam second year
########### covariates = q and cg
# select columns with cg, q and measurements of growth traits (second year after planting)
second<-pheno_all[,c(2,16,42,58)]
second.na<-na.omit(second)

second.results<-matrix(data=NA,ncol=numBootstraps+1,nrow=ncol(second.na)-2)
quantiles.second<-matrix(data=NA,ncol=2,nrow=ncol(second.na)-2)

for (i in 3:ncol(second.na)){
  second.results[i-2,1]<-getVarExplained(second.na[,i],second.na$CG,second.na$q)
  for(r in 1:numBootstraps){
    bs <- sample(1:length(second.na[,i]),replace=TRUE);
    second.results[i-2,r+1]<-getVarExplained(second.na[bs,i],second.na$CG[bs],second.na$q[bs])
    }
  quantiles.second[i-2,]<-quantile(second.results[i-2,2:ncol(second.results)],probs=c(0.025, 0.975))
}

quantiles.second<-as.data.frame(quantiles.second)
colnames(quantiles.second)<-c("quantile_2.5","quantile_97.5")
quantiles.second$orig<-second.results[,1]
rownames(quantiles.second)<-colnames(second.na[3:ncol(second.na)])
write.table(quantiles.second,file="my_filename_5.txt",quote=FALSE,col.names=TRUE,row.names=TRUE,sep="\t")


########## leaf traits
########## covariates = cg, q and year
getVarExplained4<-function(x,cg,year,q){
  orig<-var(x)
  res1<-resid(lm(x~cg*year))
  var1<-var(res1)
  res2<-resid(lm(res1~scale(q)))
  var2<-var(res2)
  ratio2<-var2/var1
  varExpl<-1-ratio2
  return(varExpl);
}

# select columns with cg, year, q and measurements of leaf morphology traits
leaf<-pheno_all[,c(2,3,16,95:96)]
leaf.na<-na.omit(leaf)

leaf.results<-matrix(data=NA,ncol=numBootstraps+1,nrow=ncol(leaf.na)-3)
quantiles.leaf<-matrix(data=NA,ncol=2,nrow=ncol(leaf.na)-3)

for (i in 4:ncol(leaf.na)){
  leaf.results[i-3,1]<-getVarExplained4(leaf.na[,i],leaf.na$CG,leaf.na$planting_year,leaf.na$q)
  for(r in 1:numBootstraps){
    bs <- sample(1:length(leaf.na[,i]),replace=TRUE);
    leaf.results[i-3,r+1]<-getVarExplained4(leaf.na[bs,i],leaf.na$CG[bs],leaf.na$planting_year[bs],leaf.na$q[bs])
   }
  quantiles.leaf[i-3,]<-quantile(leaf.results[i-3,2:ncol(leaf.results)],probs=c(0.025, 0.975))
}

quantiles.leaf<-as.data.frame(quantiles.leaf)
colnames(quantiles.leaf)<-c("quantile_2.5","quantile_97.5")
quantiles.leaf$orig<-leaf.results[,1]
rownames(quantiles.leaf)<-colnames(leaf.na[4:ncol(leaf.na)])
write.table(quantiles.leaf,file="my_filename_6.txt",quote=FALSE,col.names=TRUE,row.names=TRUE,sep="\t")








