rm(list=ls())

args = commandArgs(trailingOnly=TRUE)
# command-line arguments are the folder with the GEMMA results for a specific phenotypic trait and the prefix of the result filenames

cat("\n##########################################################################################################")
cat(paste("\nProcessing files for", args[2], sep=" "))

library("stringr")

setwd(args[1])
# get date from folder name
date<-substring(args[1], str_locate(args[1], "_bslmm_")[1]-8, str_locate(args[1], "_bslmm_")[1]-1)
prefix<-args[2]
# number of MCMC chains run in GEMMA
n_repl<-"10repl"
chains<-c("01st","02nd","03rd","04th","05th","06th","07th","08th","09th","10th")

# import .hyp files produced by GEMMA
cat("\nReading hyp files")

for (ch in chains){
  cat("\n", ch)
  readDir<-file.path(getwd())
  fileNames<-list.files(path=readDir, pattern="raspberry.hyp.txt", full.names=TRUE)
  for (i in 1:length(fileNames)){
    dfName<-paste(prefix, ch, "raspberry.hyp", sep="_")
    f<-fileNames[grep(paste(prefix, ch, "raspberry.hyp.txt", sep="_"), fileNames)]
    assign(dfName, read.table(f, header=TRUE))
  }
}

# to do a downsample of the steps, keeping only one every 10 rows
cat("\nDowsampling")
hyplist<-ls()[grep(".hyp",ls())]
for (i in 1:length(hyplist)){
  df<-get(hyplist[i])
  df2<-df[seq(1, nrow(df), 10),]
  assign(paste(prefix, str_sub(hyplist[i], start=nchar(prefix)+2, end=nchar(prefix)+5), "raspberry.hyp","thinned", sep="_"), df2)
}

# to concatenate the downsamples chains
thinlist<-ls()[grep("_thinned",ls())]
one.chain<-rbind(get(paste(prefix,"01st","raspberry.hyp_thinned",sep="_"))
                 ,get(paste(prefix,"02nd","raspberry.hyp_thinned",sep="_"))
                 ,get(paste(prefix,"03rd","raspberry.hyp_thinned",sep="_"))
                 ,get(paste(prefix,"04th","raspberry.hyp_thinned",sep="_"))
                 ,get(paste(prefix,"05th","raspberry.hyp_thinned",sep="_"))
                 ,get(paste(prefix,"06th","raspberry.hyp_thinned",sep="_"))
                 ,get(paste(prefix,"07th","raspberry.hyp_thinned",sep="_"))
                 ,get(paste(prefix,"08th","raspberry.hyp_thinned",sep="_"))
                 ,get(paste(prefix,"09th","raspberry.hyp_thinned",sep="_"))
                 ,get(paste(prefix,"10th","raspberry.hyp_thinned",sep="_")))

# to calculate product of PVE and PGE
one.chain$heritability<-one.chain$pve*one.chain$pge

write.table(one.chain, file=paste(date, prefix, "raspberry_one.chain.txt",sep="_"), quote=FALSE, row.names=FALSE, col.names=TRUE)


