#!/bin/bash

# example for one MCMC chain
# see GEMMA manual for more information: http://www.xzlab.org/software/GEMMAmanual.pdf

cd /data3/programs

# run GEMMA with residuals obtained from a script like example_regression_q_cg.r
./gemma -g /data3/admix_mapping/all_chr_weighted_ancestry_all_indv.txt \
-p /data3/admix_mapping/all_traits/pheno_C16_resid_q_cg.txt \
-bslmm 1 \
-o bslmm_C16_resid_q_cg_maf0.05_10millions_01st_raspberry \
-w 2000000 -s 10000000 \
-maf 0.05





