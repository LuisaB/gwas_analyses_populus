#!/bin/bash

# see GEMMA manual for more information: http://www.xzlab.org/software/GEMMAmanual.pdf

cd /data3/programs

# run lmm in GEMMA - first create relatedness matrix
./gemma -g /data3/admix_mapping/all_chr_weighted_ancestry_all_indv.txt \
-p /data3/admix_mapping/all_traits/pheno_avg_leaf_area_resid_q_cg_year.txt \
-gk 1 \
-o centrd_relatedness_matrix_leaf_area_resid_q_cg_year_maf0.05_raspberry \
-maf 0.05

# run lmm
./gemma -g /data3/admix_mapping/all_chr_weighted_ancestry_all_indv.txt \
-p /data3/admix_mapping/all_traits/pheno_avg_leaf_area_resid_q_cg_year.txt \
-k /data3/programs/output/centrd_relatedness_matrix_leaf_area_resid_q_cg_year_maf0.05_raspberry.cXX.txt \
-lmm 2 \
-o lmm2_leaf_area_resid_q_cg_year_maf0.05_raspberry \
-maf 0.05

