setwd("/my_directory/")

# file listing all directories from which results should be imported
folders<-read.table("/list_directories_with_results.txt", header=FALSE)
# file listing all prefixes for results that should be imported
prefixes<-read.table("list_prefixes_results.txt", header=FALSE)

cat("\nImporting files with combined chain")

library("stringr")
library("RColorBrewer")

# reading files containing "raspberry_one.chain" in filename - these files are obtained by running write_one.chain.r
for (i in 1:dim(folders)[1]){
   cat(paste("\n", prefixes[i,1], "\n", sep=" "))
   prefix<-prefixes[i, 1]
   n_repl<-"10repl" # number of MCMC chains run
   chains<-c("01st","02nd","03rd","04th","05th","06th","07th","08th","09th","10th")
   readDir<-as.character(folders[i, 1])
   fileNames<-list.files(path=readDir, pattern="raspberry_one.chain", full.names=TRUE)
   dfName<-paste(prefix, "one.chain", sep="_")
   f<-fileNames[grep(paste(prefix, "raspberry_one.chain",sep="_"), fileNames)]
   assign(dfName, read.table(f, header=TRUE))
}

# get all objects containing GEMMA results
chainList<-ls()[grep("one.chain",ls())]
chainNames<-NA
for (j in 1:length(chainList)){
  # get shorter names to be then used for plotting
  chainNames[j]<-substring(chainList[j], str_locate(chainList[j], "bslmm_")[1]+6, str_locate(chainList[j], "_resid")[2]-6)
}

# to calculate n_gamma total mass at 0, total mass for heritability <1% and median heritability
n_gamma.mass<-numeric()
herit.mass<-numeric()
herit.median<-numeric()
for (i in 1:length(chainList)){
  df<-get(chainList[i])
  n_gamma.mass[i]<-sum(df$n_gamma==0)/length(df$n_gamma)
  herit.mass[i]<-sum(df$heritability<0.01)/length(df$n_gamma)
  herit.median[i]<-median(df$heritability)
}

# to put this info into a single dataframe:
all.mass<-as.data.frame(cbind(n_gamma.mass,herit.mass,herit.median))
rownames(all.mass)<-chainNames
colnames(all.mass)<-c("n_gamma.mass","herit.mass","herit.median")
write.table(all.mass, file="all_traits_stats.txt", quote=FALSE, col.names=TRUE, row.names=TRUE, sep="\t")









