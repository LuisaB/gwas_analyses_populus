#!/bin/bash

# example for one MCMC chain
# see GEMMA manual for more information: http://www.xzlab.org/software/GEMMAmanual.pdf

cd /data3/programs

# run GEMMA with raw phenotypes and -notsnp
./gemma -g /data3/admix_mapping/all_chr_weighted_ancestry_all_indv_q_cg.txt \
-p /data3/admix_mapping/raw_phenotypes/raw_pheno_C10_Ac_St.txt \
-bslmm 1 \
-o bslmm_C10_maf0.05_notsnp2_10millions_01st_raspberry \
-w 2000000 -s 10000000 \
-notsnp
