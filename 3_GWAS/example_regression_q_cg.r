rm(list=ls())
setwd("/my_path/")

# import file with info on common garden location
cg<-read.table("cg_location.txt",header=TRUE)

# import file with phenotypic data and genome-wide ancestry (q)
pheno_all<-read.table("data_for_mapping.txt", header=TRUE)

# select columns with phytochemical traits
chem<-cbind(cg, pheno_all$q, pheno_all[,c(128:163,165,167)])
colnames(chem)<-c("ID", "CG", "q", colnames(pheno_all[,c(128:163,165,167)]))
# remove rows with NAs
chem.na<-na.omit(chem)

# calculate residuals for all phytochemical traits
for (i in 4:ncol(chem.na)){
  capture.output(summary(lm(chem.na[,i] ~ scale(chem.na$q_entropy) * chem.na$CG)), file=paste(colnames(chem.na)[i], "models.txt", sep="_"))
  write.table(resid(lm(na.omit(chem.na[,i]) ~ scale(chem.na$q_entropy) * chem.na$CG)), file=paste("pheno", colnames(chem.na)[i], "resid_interact_q_cg.txt", sep="_"), quote=FALSE, row.names=FALSE, col.names=FALSE)
}




