setwd("/my_directory/")

# read file with phenotypic measurements
pheno<-read.table("data_for_mapping.txt",header=TRUE)

# to select only columns of interest (common garden location, genome-wide admixture proportion q and columns with phenotypic traits to covert to binary traits):
chem<-na.omit(pheno[,c(2,16,142,145,147:149,152:155,158:160)])
# replace with 1 all values different from 0
binary<-chem
for (i in 1:nrow(binary)){
  for (j in 3:ncol(binary)){
  if (chem[i,j]==0) binary[i,j]<-0
  else if (chem[i,j]>0) binary[i,j]<-1
  }
}

# write a separate file for each column containing binary traits
for (i in 1:ncol(binary)){
  write.table(binary[,i],file=paste("raw_pheno_binary_", colnames(binary)[i], ".txt",sep=""), quote=FALSE, col.names=FALSE, row.names=FALSE)
}

# to get the residuals from regressing out covariates:
for (i in 3:ncol(binary)){
  capture.output(summary(glm(binary[,i] ~ scale(binary$q_entropy) * binary$CG, family="binomial"))
                 , file=paste(colnames(binary[i]),"binary_data_residuals_q_cg_summary_models.txt"
                 , sep="_"))
  write.table(resid(glm(binary[,i] ~ scale(binary$q_entropy) * binary$CG, family="binomial"))
              ,file=paste("pheno", colnames(binary[i]), "binary_resid_q_cg.txt",sep="_")
              , quote=FALSE, row.names=FALSE, col.names=FALSE)
}











