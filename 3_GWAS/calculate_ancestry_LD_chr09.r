setwd("/my_directory/ld_estimation")

library("LDheatmap")
library("RColorBrewer")
require("grid")
library("stringr")

# import the weighted ancestries
rasp<-read.table("all_chr_weighted_ancestry_all_indv.txt", header=FALSE)

# get the position of the sites used:
rasp_pos<-as.data.frame(str_split_fixed(rasp$V1, ":", 2))
rasp_pos$V3<-NA
for (i in 1:length(rasp_pos$V1)){
  if (rasp_pos$V1[i]=="1") rasp_pos$V3[i]<-"Chr01"
  else if (rasp_pos$V1[i]=="2") rasp_pos$V3[i]<-"Chr02"
  else if (rasp_pos$V1[i]=="3") rasp_pos$V3[i]<-"Chr03"
  else if (rasp_pos$V1[i]=="4") rasp_pos$V3[i]<-"Chr04"
  else if (rasp_pos$V1[i]=="5") rasp_pos$V3[i]<-"Chr05"
  else if (rasp_pos$V1[i]=="6") rasp_pos$V3[i]<-"Chr06"
  else if (rasp_pos$V1[i]=="7") rasp_pos$V3[i]<-"Chr07"
  else if (rasp_pos$V1[i]=="8") rasp_pos$V3[i]<-"Chr08"
  else if (rasp_pos$V1[i]=="9") rasp_pos$V3[i]<-"Chr09"
  else if (rasp_pos$V1[i]=="10") rasp_pos$V3[i]<-"Chr10"
  else if (rasp_pos$V1[i]=="11") rasp_pos$V3[i]<-"Chr11"
  else if (rasp_pos$V1[i]=="12") rasp_pos$V3[i]<-"Chr12"
  else if (rasp_pos$V1[i]=="13") rasp_pos$V3[i]<-"Chr13"
  else if (rasp_pos$V1[i]=="14") rasp_pos$V3[i]<-"Chr14"
  else if (rasp_pos$V1[i]=="15") rasp_pos$V3[i]<-"Chr15"
  else if (rasp_pos$V1[i]=="16") rasp_pos$V3[i]<-"Chr16"
  else if (rasp_pos$V1[i]=="17") rasp_pos$V3[i]<-"Chr17"
  else if (rasp_pos$V1[i]=="18") rasp_pos$V3[i]<-"Chr18"
  else if (rasp_pos$V1[i]=="19") rasp_pos$V3[i]<-"Chr19"
}

# remove the columns with positions and alleles:
rasp<-rasp[,c(4:475)]
colnames(rasp)<-paste("ind",1:472,sep="_")
# cbind columns with positions written in good form:
raspChr<-as.data.frame(cbind(rasp_pos[,c(3,2)],rasp))
colnames(raspChr)<-c("chr","pos",colnames(rasp))
 
# separate per chromosome:
chr01<-raspChr[raspChr$chr=="Chr01",]
chr02<-raspChr[raspChr$chr=="Chr02",]
chr03<-raspChr[raspChr$chr=="Chr03",]
chr04<-raspChr[raspChr$chr=="Chr04",]
chr05<-raspChr[raspChr$chr=="Chr05",]
chr06<-raspChr[raspChr$chr=="Chr06",]
chr07<-raspChr[raspChr$chr=="Chr07",]
chr08<-raspChr[raspChr$chr=="Chr08",]
chr09<-raspChr[raspChr$chr=="Chr09",]
chr10<-raspChr[raspChr$chr=="Chr10",]
chr11<-raspChr[raspChr$chr=="Chr11",]
chr12<-raspChr[raspChr$chr=="Chr12",]
chr13<-raspChr[raspChr$chr=="Chr13",]
chr14<-raspChr[raspChr$chr=="Chr14",]
chr15<-raspChr[raspChr$chr=="Chr15",]
chr16<-raspChr[raspChr$chr=="Chr16",]
chr17<-raspChr[raspChr$chr=="Chr17",]
chr18<-raspChr[raspChr$chr=="Chr18",]
chr19<-raspChr[raspChr$chr=="Chr19",]

# remove again the first two columns with the positions (chr and bp positions):
chr.list<-ls()[grep("chr",ls())]
for (i in chr.list){
  df<-get(i)
  new.df<-df[,3:474]
  assign(i,new.df)
}

# obtain the position used in RASPberry for chr09
pos.chr09<-rasp_pos[rasp_pos$V3=="Chr09",c(3,2)]
colnames(pos.chr09)<-c("chr","pos")
pos.chr09$pos<-as.numeric(as.character(pos.chr09$pos))
pos.map<-pos.chr09$pos

# calculate LD
ld.chr09<-matrix(nrow=length(chr09$ind_1),ncol=length(chr09$ind_1))
ld.chr09[lower.tri(ld.chr09)]<-NA # put NA in the part of the matrix below the diagonal
for (i in 1:length(chr09$ind_1)){
  for (j in i:length(chr09$ind_1)){ # loop over columns for which j>i
    ld.chr09[i,j]<-(cor(as.numeric(chr09[i,]),as.numeric(chr09[j,])))^2
  }
}
# replace with NA the 1s on the diagonal, othewise they appear in the plot:
ld.chr09[row(ld.chr09)==col(ld.chr09)]<-NA
# write to a file the matrix with LD
write.table(ld.chr09, file="ld.chr09_raspberry.txt", quote=FALSE, row.names=FALSE, col.names=FALSE)

# calculate physical distance between the markers
phys.dist<-matrix(nrow=length(chr09$ind_1),ncol=length(chr09$ind_1))
phys.dist[lower.tri(phys.dist)]<-NA
for (i in 1:length(pos.chr09$pos)){
  for (j in i:length(pos.chr09$pos)){
    phys.dist[i,j]<-pos.chr09[j,2]-pos.chr09[i,2]
  }
}
write.table(phys.dist, file="phys.dist_raspberry_chr09.txt", quote=FALSE, row.names=FALSE, col.names=FALSE)

# plots
png(file="LD_decay_raspberry_all_vs_all_chr09.png", height=10, width=10, units="in", res=c(72*3))
par(mfrow=c(1,1),mar=c(8,7,5,4))
plot(phys.dist,ld.chr09,xlab="",ylab="",main="", cex=0.8, cex.lab=2
     , xaxt="n", yaxt="n", las=2)
axis(side=1, cex.axis=2, at=c(0,2000000,4000000,6000000,8000000,10000000,12000000), labels=rep("",7))
mtext(side=1, text=c(0,"",4000000,"",8000000,"",12000000)
      , at=c(0,2000000,4000000,6000000,8000000,10000000,12000000), line=2, cex=2)
mtext(side=1, text="Physical distance", line=4.5, cex=2)
axis(side=2, cex.axis=2, las=2)
mtext(side=2, text="Ancestry LD", line=4, cex=2)
text(x=(max(phys.dist,na.rm=TRUE)-(0.1*max(phys.dist,na.rm=TRUE))),y=0.98,labels=paste("N sites",dim(pos.chr09)[1],sep="="),cex=2)
dev.off()

png(file="LD_triangle_raspberry_all_vs_all_chr09.png", height=10, width=10, units="in", res=c(72*3))
ld.plot<-LDheatmap(ld.chr09, LDmeasure="r", title="", add.map=TRUE, add.key=TRUE
          , genetic.distances=pos.map, color=rev(brewer.pal(n=9,name="YlGnBu")))
p1 <- editGrob(ld.plot$LDheatmapGrob,gPath("heatMap", "title"), gp = gpar(cex = 1.8))
p1 <- editGrob(p1, gPath("geneMap","title"), gp = gpar(cex =2.2))
p1 <- editGrob(p1, gPath("Key", "title"),gp = gpar(cex = 1.9))
p1 <- editGrob(p1, gPath("Key", "labels"),gp = gpar(cex = 1.75))
grid.newpage()
grid.draw(p1)
dev.off()







