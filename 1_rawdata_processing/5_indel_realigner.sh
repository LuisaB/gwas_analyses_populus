#!/bin/bash

module add UHTS/Analysis/GenomeAnalysisTK/3.4.46;

filenames_endtoend436='/my_path/names_trichomapping_endtoend_filterMQ10_RG_lane436.txt'
filelines_endtoend436=`cat $filenames_endtoend436`

for line_endtoend436 in $filelines_endtoend436
do
   java -Xmx6g -jar $GATK_PATH/GenomeAnalysisTK.jar \
   -T IndelRealigner \
   -R /path_to_reference_genome/reference_genome.fasta \
   -I $line_endtoend436 \
   -targetIntervals /my_path/target_creator_trichomapping_endtoend_filterMQ10_RG.intervals \
   -o ${line_endtoend436%_MQ10_RG.bam}_realigned.bam
done

module rm UHTS/Analysis/GenomeAnalysisTK/3.4.46
