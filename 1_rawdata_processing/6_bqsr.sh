#!/bin/bash

cd /home/programs/GenomeAnalysisTK-3.4-46

filenames_realignedbam436='/my_path/realigned_bamfiles_436_for_recal/names_trichomapping_endtoend_realignedbam436.txt'
filelines_realignedbam436=`cat $filenames_realignedbam436`

for line_realignedbam436 in $filelines_realignedbam436
do
java -Xmx4g -jar GenomeAnalysisTK.jar \
    -T BaseRecalibrator \
   -R /path_to_reference_genome/reference_genome.fasta \
    -I $line_realignedbam436 \
    -knownSites /my_path/test_bqsr/cleantest1_header.vcf \
    -o ${line_realignedbam436%_realigned.bam}_recal.table \
    --log_to_file ${line_realignedbam436%_realigned.bam}_bqsr1.log \
    -nct 2

java -Xmx4g -jar GenomeAnalysisTK.jar \
   -T PrintReads \
   -R /path_to_reference_genome/reference_genome.fasta \
   -I $line_realignedbam436 \
   -BQSR ${line_realignedbam436%_realigned.bam}_recal.table \
   -o ${line_realignedbam436%_realigned.bam}_recal.bam \
   --log_to_file ${line_realignedbam436%_realigned.bam}_recal.log \
   -nct 2

java -Xmx4g -jar GenomeAnalysisTK.jar \
    -T BaseRecalibrator \
   -R /path_to_reference_genome/reference_genome.fasta \
    -I $line_realignedbam436 \
    -knownSites /my_path/test_bqsr/cleantest1_header.vcf \
    -BQSR ${line_realignedbam436%_realigned.bam}_recal.table \
    -o ${line_realignedbam436%_realigned.bam}_post_recal.table \
    --log_to_file ${line_realignedbam436%_realigned.bam}_bqsr2.log \
    -nct 2

java -Xmx4g -jar GenomeAnalysisTK.jar \
    -T AnalyzeCovariates \
    -R /path_to_reference_genome/reference_genome.fasta \
    -before ${line_realignedbam436%_realigned.bam}_recal.table \
    -after ${line_realignedbam436%_realigned.bam}_post_recal.table \
    -plots ${line_realignedbam436%_realigned.bam}_plots.pdf \
    --log_to_file ${line_realignedbam436%_realigned.bam}_plots.log
done
