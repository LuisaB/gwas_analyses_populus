#!/bin/bash

# demultiplexing

cd /home/programs/eautils/ea-utils.1.1.2-806

./fastq-multx /path_to_barcode_file/barcode_file /path_to_rawdata/UO_C436_1.fastq -o /path_to_output/UO_C436_1_.%.fq


# quality check after demultiplexing and quality trimming

FILES=/path_to_output/UO_C436_1_*.fq

for f in $FILES
do
	fastqc $f
	perl condetri_v2.2.pl -fastq1=$f -prefix=$f -hq=25 -lq=10 -frac=0.80 -lfrac=0.1 -minlen=50 -mh=5 -ml=1 -sc=33
done


# quality check after trimming

fastqc /path_to_output/*trim.fastq
