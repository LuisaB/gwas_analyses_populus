#!/bin/bash

# build index for reference genome
bowtie2-build /path_to_reference_genome/reference_genome.fasta reference_genome.fasta

# create .dict and .fai files for reference genome
picard-tools CreateSequenceDictionary R=reference_genome.fasta O=reference_genome.fasta.dict

samtools faidx reference_genome.fasta

# alignment to reference genome
cd /path_to_fastq_files_after_quality_trimming

filenames_fastq436='/path_to_file/fastq_names436.txt'
filelines_fastq436=`cat $filenames_fastq436`

for line_fastq436 in $filelines_fastq436
do
    bowtie2 -p 4 --very-sensitive -x /path_to_reference_genome/reference_genome.fasta \
   -U $line_fastq436 \
   --phred33 -t -S ${line_fastq436%.fq_trim.fastq}_endtoend.sam
done

# convert SAM to BAM
filenames_endtoend_sam436='/path_to_file/names_endtoend_sam436.txt'
filelines_endtoend_sam436=`cat $filenames_endtoend_sam436`

for line_endtoend_sam436 in $filelines_endtoend_sam436
do
    samtools view -Sb -h -t /path_to_reference_genome/reference_genome.fasta.fai \
    -o ${line_endtoend_sam436%.sam}.bam \
    $line_endtoend_sam436
done

# remove reads with mapping quality <10
filenames_endtoend_bam436='/path_to_file/names_endtoend_bam436.txt'
filelines_endtoend_bam436=`cat $filenames_endtoend_bam436`

for line_endtoend_bam436 in $filelines_endtoend_bam436
do
    samtools view -b -h -q 10 \
    $line_endtoend_bam436 \
    -o ${line_endtoend_bam436%.bam}_filteredMQ10.bam
done




