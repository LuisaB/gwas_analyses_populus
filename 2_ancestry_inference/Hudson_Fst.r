rm(list=ls())

setwd("/my_path/")

# read results from entropy
est.p<-read.csv("entropy_k2_q_3ch.pest.txt")

n_loci <- 127322

plot(est.p$mean[1:n_loci], est.p$mean[(n_loci+1):nrow(est.p)])
plot(est.p$mean[1:n_loci], est.p$mean[(n_loci+1):nrow(est.p)], xlab="p (species 0)", ylab="p (species 1)")
dev.print(pdf, "p_sp0_sp1.pdf")

hudsonFst<-function(p1=NA, p2=NA, locusspecific=F){
  p1.comp <- 1 - p1
  p2.comp <- 1 - p2
  if(locusspecific==T){
    return(1 - (p1 * p1.comp + p2 * p2.comp)/(p1 * p2.comp + p2 * p1.comp)) # this is fst
  }
  mean(1 - (p1 * p1.comp + p2 * p2.comp)/(p1 * p2.comp + p2 * p1.comp)) # this is fst
}
fst<-hudsonFst(est.p$mean[1:n_loci], est.p$mean[(n_loci+1):nrow(est.p)], locusspecific=T)
hist(fst)
dev.print(pdf, "hudson_fst.pdf")
summary(fst)





