#!/bin/bash

cd /path_to_filtered_vcf

# run vcftools to get plink format
vcftools --vcf filtered_vcf.vcf --out filtered_vcf_plink --plink

# run plink
cd /home/programs/plink-1.07-x86_64
./plink --file filtered_vcf_plink --recode12 --out filtered_vcf_after_plink

# run admixture
cd /home/programs/admixture_linux-1.3.0
./admixture -B filtered_vcf_after_plink.ped 2
